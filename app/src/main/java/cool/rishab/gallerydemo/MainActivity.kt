package cool.rishab.gallerydemo

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity(), SelectImage.OnSelectImageListener {

    //getting path of image using this method from onSelectImageListener interface
    override fun onImageSelected(path: String) {
        val file = File(path)
        image_main.setImageURI(Uri.fromFile(file))
    }

    lateinit var selectImage: SelectImage
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //created object of SelectImage class to access methods of that class
        selectImage = SelectImage(this).apply {
            selectImage(this@MainActivity)
        }

        btn_camera.setOnClickListener { selectImage.selectImageFromCamera(this) }
        btn_chooser.setOnClickListener { selectImage.openChooser(this) }
        btn_gallery.setOnClickListener { selectImage.selectImageFromGallery(this) }
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //SelectImage onActivityResult callback method is called here
        selectImage.onActivityResult(requestCode, resultCode, data)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //SelectImage onRequestPermissionResult method is called here
        selectImage.onRequestPermissionResult(this, requestCode, permissions, grantResults)
    }
}
