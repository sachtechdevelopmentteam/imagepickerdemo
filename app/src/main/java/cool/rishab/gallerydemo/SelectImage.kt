package cool.rishab.gallerydemo

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.provider.MediaStore.Images
import android.support.annotation.RequiresApi
import kotlinx.android.synthetic.main.gallery_dialog.*
import java.io.ByteArrayOutputStream


class SelectImage(val context: Context) {
    var CAMERA_REQUEST_CODE = 10
    var GALLERY_REQUEST_CODE = 20
    var bitmap: Bitmap? = null
    var path: String? = null
    var selectImageListener: SelectImage.OnSelectImageListener? = null
    var dialog: Dialog? = null

    //Method to select Image from camera
    fun selectImageFromCamera(activity: Activity) {
        //check permissions of camera and write external storage
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (checkPermissions(
                    arrayOf(
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                )
            )
            //if permissions are available,it opens the camera
                openCamera(activity)

            //otherwise it goes to onRequestPermissionResult
            else
                activity.requestPermissions(
                    arrayOf(
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), CAMERA_REQUEST_CODE
                )
    }

    //method to open camera
    private fun openCamera(activity: Activity) {
        val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)

        //after capturing image,it gives the result in onActivityResult method
        activity.startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    //method to select image from gallery
    fun selectImageFromGallery(activity: Activity) {

        //check permissions for read external storage
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (checkPermissions(
                    arrayOf(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                )
            )
            //if permissions are available, it goes to the gallery
                openGallery(activity)
            else
            //otherwise it returns the permission result into onRequestPermissionResult method
            {
                activity.requestPermissions(
                    arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                    GALLERY_REQUEST_CODE
                )
            }
    }

    //method to open the gallery
    private fun openGallery(activity: Activity) {
        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //after selecting image, it gives the result in onActivityResult method
        activity.startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    //method to select image from chooser
    fun openChooser(activity: Activity) {
        createDialog(activity)
    }

    private fun createDialog(activity: Activity) {
        dialog = Dialog(activity)
        dialog?.setContentView(R.layout.gallery_dialog)
        dialog?.dialog_text_camera!!.setOnClickListener { selectImageFromCamera(activity) }
        dialog?.dialog_text_gallery!!.setOnClickListener { selectImageFromGallery(activity) }
        dialog?.show()
    }

    //listener to give the result to the activity
    fun selectImage(selectListener: OnSelectImageListener) {
        selectImageListener = selectListener
    }

    //after capturing image or selecting image from gallery,it gives the result here
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data != null) {
            if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
                val bitmap = data?.extras?.get("data") as Bitmap
                val uri = getImageUri(context, bitmap)
                path = FileUtils.getPath(context, uri)
            } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
                val uri = data?.data
                path = FileUtils.getPath(context, uri!!)
            }

            selectImageListener!!.onImageSelected(path!!)
        }
        if (dialog != null) {
            dialog?.dismiss()
        }
    }

    //method to convert bitmap into uri
    private fun getImageUri(context: Context, photo: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = Images.Media.insertImage(context.contentResolver, photo, "Title", null)
        return Uri.parse(path)
    }

    //after requesting for permissions,it gives the permission result here
    fun onRequestPermissionResult(
        activity: Activity,
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == CAMERA_REQUEST_CODE)
            selectImageFromCamera(activity)
        else if (requestCode == GALLERY_REQUEST_CODE)
            selectImageFromGallery(activity)
    }

    //to send the path of image to the activity
    interface OnSelectImageListener {
        fun onImageSelected(path: String)
    }

}


